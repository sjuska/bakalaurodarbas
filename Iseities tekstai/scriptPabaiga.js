var setup = function(args, ctx, goo){
	
	ctx.handleClick = function(event){
		ctx.entity.soundComponent.sounds[0].play();
		ctx.entity.hide();
		ctx.world.by.name('Pradzia').first().show();
	};
	
	ctx.handleClick2 = function(event){
		ctx.entity.soundComponent.sounds[0].play();
		ctx.entity.hide();
		ctx.worldData.startGame();
	};
	
  var button = document.getElementById('grizti');
  button.addEventListener('click', ctx.handleClick);
  var button2 = document.getElementById('kartoti');
  button2.addEventListener('click', ctx.handleClick2);
}
 
var cleanup = function(args, ctx, goo){
  var button = document.getElementById('grizti');
  button.removeEventListener('click', ctx.handleClick);
  var button2 = document.getElementById('kartoti');
  button2.removeEventListener('click', ctx.handleClick2);
}