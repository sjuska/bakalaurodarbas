var setup = function(args, ctx, goo){
	ctx.handleClick = function(event){
		ctx.entity.soundComponent.sounds[0].play();
		ctx.entity.hide();
		ctx.world.by.name('Pradzia').first().show();
	};
	
  var button = document.getElementById('atgal');
  button.addEventListener('click', ctx.handleClick);
}

var cleanup = function(args, ctx, goo){
  var button = document.getElementById('atgal');
  button.removeEventListener('click', ctx.handleClick);
}