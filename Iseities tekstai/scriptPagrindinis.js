'use strict';

var setup = function(args, ctx, goo) {
	var colors = ['DeepSkyBlue', 'LimeGreen', 'Gold', 'OrangeRed', 'Teal',
		'DarkViolet', 'DeepPink', 'Red', 'Blue', 'White'];
	
	var score = 0;
	var moves = 0;
	var sum = 0;
	
	var textures = [];
	var grid = [];
	
	var d = 256;
	var f = 120;
	
	for(var t=1; t<=10; t++) {
		var canvas = document.createElement('canvas');
		canvas.width = d;
		canvas.height = d;
		var c = canvas.getContext('2d');

		c.beginPath();
		c.rect(0, 0, d, d);
		c.fillStyle = colors[t-1];
		c.fill();
		c.lineWidth = 1;
		c.strokeStyle = 'Black';
		c.stroke();
      
		c.font= f+"pt Arial Black";
		c.fillStyle="Black";
		c.textAlign = 'center';
		c.fillText(""+t,d/2,d/2+f/2);

		textures[t] = new goo.Material(goo.ShaderLib.uber);
		textures[t].uniforms.materialAmbient = [0.7,0.7,0.7,0];
		textures[t].setTexture('DIFFUSE_MAP', new goo.Texture(canvas));
	}
	
	function newBox(x,y,nr) {
		nr = nr || Math.ceil(Math.random()*4);
		return {
			nr: nr,
			e: ctx.world.createEntity('box', new goo.Box(), textures[nr], [x,y,0]).addToWorld()
		};
	}
	
	function updateHUD(){
	  	var lentele = document.getElementById('taskai');
		lentele.innerHTML = Math.round(score);
	}
	
  	ctx.worldData.startGame = function(){
		ctx.world.by.name('HUD').first().show();
		ctx.world.gooRunner.addEventListener('mousedown', ctx.click);	
		ctx.world.gooRunner.addEventListener('touchcancel', ctx.click);
		
		score = 0;
		moves = 0;
		sum = 0;
		
		updateHUD();
		
		for(var x=0; x<5; x++) {
			grid[x] = [];
			for(var y=0; y<5; y++) {
				grid[x][y] = newBox(x,y);
			}
		}
  	};
	
	ctx.worldData.endGame = function(){
		ctx.world.gooRunner.removeEventListener('mousedown', ctx.click);
		ctx.world.gooRunner.removeEventListener('touchcancel', ctx.click);
		
		ctx.world.by.name('box').forEach(function(e) {
			e.removeFromWorld();
		});
		ctx.world.by.name('HUD').first().hide();
	};
	
	function removeBoxes(x, y, nr) {
		if(x<0 || x>4 || y<0 || y>4 || grid[x][y].removed || grid[x][y].nr != nr)
			return 0;
		var count=1;
		grid[x][y].removed = true;
		count += removeBoxes(x+1,y,nr);
		count += removeBoxes(x-1,y,nr);
		count += removeBoxes(x,y+1,nr);
		count += removeBoxes(x,y-1,nr);
		return count;
	}

	function moveDown(box, x, y, distance) {
		var coords = { y: 0 };
		var tween = new TWEEN.Tween(coords)
			.to({ y: distance }, 300)
			.easing(window.TWEEN.Easing.Quadratic.InOut)
			.onUpdate(function() {
				box.setTranslation(x,y-this.y,0);
			})
			.start();
		
		requestAnimationFrame(animate);

		function animate(time) {
			requestAnimationFrame(animate);
			TWEEN.update(time);
		}
	}
	
	function countMissingBelow(x,y) {
		var c=0;
		for(var r=y-1; r>=0; r--) {
			if(grid[x][r].removed) {
				c++;
			}
		}
		return c;
	}
	
	function compactBoard() {
		for(var x=0; x<5; x++) {
			for(var y=1; y<5; y++) {
				var tile = grid[x][y];
				if(!tile.removed) {
					var m = countMissingBelow(x,y);
					if( m>0) {
						//console.log(x, y, m);
						moveDown( grid[x][y].e, x, y, m);
						grid[x][y-m] = grid[x][y];
						grid[x][y] = {removed:true};
					}
				}
			}
			m = countMissingBelow(x,5);
			if( m>0) {
				for(var i=0; i<m; i++) {
					var b = grid[x][5-m+i]=newBox(x, 5+i);
					moveDown( b.e, x, 5+i, m);
				}
			}
		}
	}
	
	function clear() {
		for(var x=0; x<5; x++) {
			for(var y=0; y<5; y++) {
				var tile = grid[x][y];
				if(tile.removed && tile.e) {
					tile.e.removeFromWorld();
				}
			}
		}
	}

	function calculateScore(count, nr) {
		moves++;
		sum += count * nr;
		score += sum / moves;
		updateHUD();
	}
	
	function saveHighscore(value){
	  try {
		localStorage.setItem('highScore', value);
	  } catch(err){
		console.error('Išsaugoti nepavyko.');
	  }
	}
	
	function getHighscore(){
	  try {
		return parseInt(localStorage.getItem('highScore'))||0;
	  } catch(err){
		console.error('Rekordas nerastas.');
		return 0;
	  }
	}
	
	function showEnd(msg) {
		ctx.world.by.name('Pabaiga').first().show();
		var surinkta = document.getElementById('surinkta');
		if (Math.round(score) > getHighscore()){
			surinkta.innerHTML = msg+"<br>Pasiektas naujas taškų rekordas: <span style='color:red'>" + Math.round(score) + "</span>";
			saveHighscore(Math.round(score));
		}
		else {
			surinkta.innerHTML = msg+"<br>Surinkti taškai: <span style='color:red'>" + Math.round(score) + "</span><br>Surinktų taškų rekordas: <span style='color:red'>" + getHighscore();
		}
		ctx.worldData.endGame();
	}
	
	ctx.click = function(event) {
		if (!event.entity)
			return;
		else if (event.entity.name !== 'box')
			return;
		
		var t = event.entity.getTranslation();
		var col = t.x;
		var row = t.y;
		var nr = grid[col][row].nr;
		
		
		if( nr === 10) {		
			showEnd("Pergalė!");
		}
		else{
			var count = removeBoxes(col, row, nr);
			
			if( count === 1) {
				grid[col][row].removed = false;
			} 
			else if( count > 1) {
				calculateScore(count, nr);
				ctx.entity.soundComponent.sounds[0].play();
				clear();
				grid[col][row] = newBox(col,row,nr+1);
				compactBoard();
				if(checkEnd()) {
					showEnd("Baigėsi galimų ėjimų skaičius!");
				}
			}
		}
	};
	
	function getNr(x,y) {
		if( x<0 || x>4 || y<0 || y>4 )
			return 0;
		return grid[x][y].nr;
	}
	
	function checkEnd() {
		for(var x=0; x<5; x++) {
			for(var y=0; y<5; y++) {
				var tile = grid[x][y].nr;
				if( tile == getNr(x+1,y)
				||  tile == getNr(x-1,y)
				||  tile == getNr(x,y+1)
				||  tile == getNr(x,y-1))
					return false;
			}
		}
		return true;
	}
};

var cleanup = function(args, ctx, goo) {
	ctx.world.gooRunner.removeEventListener('mousedown', ctx.click);
	ctx.world.gooRunner.removeEventListener('touchcancel', ctx.click);
	
	ctx.world.by.name('box').forEach(function(e) {
		e.removeFromWorld();
	});
};

var update = function(args, ctx, goo) {

};

var parameters = [];

