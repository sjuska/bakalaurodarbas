var setup = function(args, ctx, goo){
	ctx.handleClick = function(event){
		ctx.entity.soundComponent.sounds[0].play();
		ctx.entity.hide();
		ctx.worldData.startGame();
	};
	ctx.handleClick2 = function(event){
		ctx.entity.soundComponent.sounds[0].play();
		ctx.entity.hide();
		ctx.world.by.name('Instrukcija').first().show();
	};
	
  var button = document.getElementById('pradeti');
  button.addEventListener('click', ctx.handleClick);
  var button2 = document.getElementById('info');
  button2.addEventListener('click', ctx.handleClick2);
}
 
var cleanup = function(args, ctx, goo){
  var button = document.getElementById('pradeti');
  button.removeEventListener('click', ctx.handleClick);
  var button2 = document.getElementById('info');
  button2.removeEventListener('click', ctx.handleClick2);
}